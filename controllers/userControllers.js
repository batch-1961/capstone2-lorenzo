const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//registration

module.exports.registerUser = (req,res)=>{


	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);

	let newUser = new User(
		{
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			email: req.body.email,
			password: hashedPW,
			mobileNo: req.body.mobileNo
		}
		)

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


//getting authentication/token for a user

module.exports.loginUser = (req,res)=>{

	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send({message: "User does not exist!"})

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});

			} else {
				return res.send({message: "Incorrect Password"});
			}
		}
	})

}

//authenticating user with token/ or get its own details

module.exports.loginUserSuccess = (req,res)=>{

	
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


//set a user to admin by admin user

module.exports.setAsAdmin = (req,res) => {

	// console.log(req.params.userId);
	// console.log(req.body);
	let userUpdate = {
		"isAdmin": true,
    }

	User.findByIdAndUpdate(req.params.userId,userUpdate,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}


//pangkuha lang ng id ng mga user di na need pumnta sa robo 3t

module.exports.allUser = (req,res) => {

	User.find()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
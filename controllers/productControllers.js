const Product = require("../models/Product");

const auth = require("../auth");


//get active products access all user

module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//retrieve single product

module.exports.getSingleProduct = (req,res)=>{


	console.log(req.params.productId);

	Product.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//create new product non admin
/*

module.exports.createProducts = (req,res) => {

	let newProduct = new Product(req.body)

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
};*/

//create new product  admin

module.exports.createProductsAdmin = (req,res)=>{

	let newProductAdmin = new Product(
	{
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	)

	newProductAdmin.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


//update product


module.exports.updateProducts = (req,res) => {
	console.log(req.params.productId);	
	console.log(req.body);


	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		// isActive: req.body.isActive
	}
	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

//archive product


module.exports.archiveProducts = (req,res) => {

	console.log(req.params.productId);
	console.log(req.body);

	let archive = {isActive: false};

	Product.findByIdAndUpdate(req.params.productId,archive,{new:true})

	.then(result => res.send(result))
	.catch(error => res.send(error))
};


//archive product

module.exports.activateProducts = (req,res) => {

	console.log(req.params.productId);
	console.log(req.body);

	let activate = {isActive: true};

	Product.findByIdAndUpdate(req.params.productId,activate,{new:true})

	.then(result => res.send(result))
	.catch(error => res.send(error))
};
//import the package of express & mongoose

const express = require("express");
const mongoose = require("mongoose");

// use the express.json
const app = express();
app.use(express.json());

//set the port to be used
const port = process.env.Port || 4000;

//connect the database to mongoose >> change the password accdg to your password set in mongoose >> specify the name of database after 'mongo.net/' eg. ecommerce, useNewUrlParcel support old url, useUnifiedTopology to support drivers, reuce the maintenance burden and remove confusing functionality
mongoose.connect("mongodb+srv://admin:admin123@cluster0.cnvebhq.mongodb.net/onlineMarket?retryWrites=true&w=majority",{useNewUrlParser: true, useUnifiedTopology: true});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB Connection Error."))

db.once('open', () => console.log("Connected to MongoDB."))

//routes
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);
const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders', orderRoutes);





app.listen(port,() => console.log(`Express API running at port 4000`));
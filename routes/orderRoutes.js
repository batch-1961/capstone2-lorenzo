const express = require("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");
const {verify} = auth;
const {verifyAdmin} = auth;

router.post('/createOrder', verify,orderControllers.createOrder);

router.get('/getAllOrders', verify,verifyAdmin,orderControllers.getAllOrders);

router.get('/getUserOrder', verify,orderControllers.getUserOrder);

// router.get('/getSpecificUserOrder', verify,orderControllers.getSpecificUserOrder);


module.exports = router;